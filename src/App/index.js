import React, { Component } from "react";
import { Provider } from "react-redux";


import "./index.css";
import Routes from "../Route";
import store from "../store"

class App extends Component {
  render() {
    return (
      <Provider store={ store }>
        <Routes />
      </Provider>
    );
  }
}

export default App;
