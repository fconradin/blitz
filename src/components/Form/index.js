import React, { Component } from "react";
import { RaisedButton, TextField } from "material-ui";
import { connect } from "react-redux";

import { login } from "../../store/actions/apiCalls.js";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = { email: "", password: "" };
  }

  componentDidMount() {
    if (localStorage.getItem("token")) {
      this.props.history.push("/feed");
    }
  }

  handleEmailChange = e => {
    this.setState({ email: e.currentTarget.value });
  };

  handlePasswordChange = e => {
    this.setState({ password: e.currentTarget.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    const action = login(
      this.state.email,
      this.state.password,
      this.props.history.push
    );
    this.setState({ email: "", password: "" });
    this.props.dispatch(action);
  };
  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <TextField
            onChange={this.handleEmailChange}
            type="email"
            errorText={this.props.credentialCheck}
            value={this.state.email}
            floatingLabelText="Email address"
            required={true}
          />
          <br />
          <TextField
            onChange={this.handlePasswordChange}
            type="password"
            errorText={this.props.credentialCheck}
            value={this.state.password}
            floatingLabelText="Password"
            required={true}
          />
          <br />
          <RaisedButton label="submit" type="submit" />
        </form>
      </div>
    );
  }
}

const mapsStateToProps = state => ({ credentialCheck: state.credentialCheck });

export default connect(mapsStateToProps)(Form);
