import React, { Component } from "react";
import { connect } from "react-redux";
import Divider from "material-ui/Divider";
import { List, ListItem } from "material-ui/List";
import { Avatar } from "material-ui";
import Badge from "material-ui/Badge";
import "./index.css";

import { fetchUserProfile } from "../../store/actions/apiCalls";

class UserProfile extends Component {
  componentDidMount() {
    this.props.dispatch(fetchUserProfile(this.props.history.push));
  }
  render() {
    return (
      <div className="userProfileDiv">
        <Badge
          badgeContent={this.props.numberOfBlitzes}
          primary={true}
          badgeStyle={{ top: 25, right: 25, height: 50, width: 50 }}
        >
          <Avatar src={this.props.avatar} size={150} />
        </Badge>
        <List className="userProfileList">
          <ListItem   
            secondaryText="Name"
            primaryText={this.props.name}
            disabled={true}
          />
          <ListItem
            secondaryText="Email address"
            primaryText={this.props.email}
            disabled={true}
          />
          <ListItem
            secondaryText="Number of Blitzes"
            primaryText={this.props.numberOfBlitzes}
            disabled={true}
          />
        </List>
        <Divider />
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log("profile", state.profile);
  console.log("email", state.profile.email);
  console.log("username", state.profile.username);
  console.log("blitz", state.profile.blitzs);
  const avatar = state.profile.avatar;
  const name = state.profile && state.profile.username.replace("_", " ");
  const email = state.profile && state.profile.email;
  const numberOfBlitzes = state.profile && state.profile.blitzs.length;
  return {
    avatar,
    name,
    email,
    numberOfBlitzes
  };
};

export default connect(mapStateToProps)(UserProfile);
