import React, { Component } from "react";
import { connect } from "react-redux";
import { List, ListItem } from "material-ui/List";
import Avatar from "material-ui/Avatar";
// import IconButton from "material-ui/IconButton";
// import FontIcon from "material-ui/FontIcon";
// import ActionHome from "material-ui/svg-icons/action/home";

import { fetchFeed, toggleLike } from "../../store/actions/apiCalls.js";
import "./index.css";
import { Toggle } from "material-ui";

class Feed extends Component {
  componentDidMount() {
    this.props.dispatch(fetchFeed(this.props.history.push));
  }

  handleToggle = e => {
    console.log("toggled", e.currentTarget.id);
    this.props.dispatch(toggleLike(e.currentTarget.id));
  };

  render() {
    return (
      <div>
        <List className="FeedList">
          {this.props.feed.map((item, index) => (
            <ListItem
              key={index}
              leftAvatar={<Avatar src={item.avatar} />}
              primaryText={item.username}
              secondaryText={<p>{item.content}</p>}
              secondaryTextLines={2}
              rightToggle={
                <Toggle
                  id={item.id}
                  toggled={item.toggled}
                  onToggle={this.handleToggle}
                />
              }
            />
          ))}
        </List>
      </div>
    );
  }
}

const mapStateToProps = state => {
  let feed = [];
  // console.log("user mapStatetoProps Feed", state);
  const likes = state.user.me.likes;
  for (let key in Object.keys(state.feed)) {
    if (state.user.me.follows.indexOf(state.feed[key]._user._id) > -1) {
      feed.push({
        id: state.feed[key]._id,
        avatar: state.feed[key]._user.avatar,
        username: state.feed[key]._user.username,
        content: state.feed[key].content, 
        toggled: likes.indexOf(state.feed[key]._id) > -1
      });
    }
  }
  return { feed };
};

export default connect(mapStateToProps)(Feed);
