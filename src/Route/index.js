import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Feed from "../components/Feed";
import Form from "../components/Form";
import UserProfile from "../components/UserProfile";

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Form} />
      <Route exact path="/feed/" component={Feed} />
      <Route exact path="/user/:userId/" component={UserProfile}/>
    </Switch>
  </BrowserRouter>
);

export default Routes;