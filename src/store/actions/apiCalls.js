import {
  wrongCredentials,
  storeUserAndToken,
  readUserToken,
  storeFeed,
  storeProfile,
  likePost,
  storeMe
} from "./actions.js";
const devUrl = "https://propulsion-blitz.herokuapp.com";

const login = (email, password, pushFn) => (dispatch, getState) => {
  const headers = new Headers({ "Content-Type": "application/json" });
  const body = JSON.stringify({ email, password });
  const config = { headers, method: "POST", body };
  fetch("https://propulsion-blitz.herokuapp.com/api/login", config)
    .then(res => {
      if (res.status !== 200) {
        return "error";
      } else {
        return res.json();
      }
    })
    .then(user => {
      if (user === "error") {
        dispatch(wrongCredentials());
      } else {
        // console.log("api calls login", user, user.token);
        dispatch(storeUserAndToken(user, user.token));
        pushFn("/feed");
      }
    });
};

const returnFetchConfig = (dispatch, getState, pushFn, method) => {
  if (!localStorage.getItem("token")) {
    pushFn("/");
    return undefined;
  }

  if (!getState().user.token) {
    // console.log("fetch config no token in state");
    dispatch(readUserToken());
  }
  const token = getState().user.token;
  // console.log("fetch config", getState().user.token);
  const myHeaders = new Headers({
    Authorization: `Bearer ${token}`
  });

  const config = {
    method: method,
    headers: myHeaders
  };
  return config;
};

const fetchUserProfile = pushFn => (dispatch, getState) => {
  /* 
* fetches the profile of the chosen user
*/

  const config = returnFetchConfig(dispatch, getState, pushFn, "GET");
  if (config) {
    fetch(`${devUrl}/api/users/5940d9531380402f7637e09f`, config)
      .then(res => {
        return res.json();
      })
      .then(profile => {
        dispatch(storeProfile(profile));
      });
  }
};

const fetchMe = pushFn => (dispatch, getState) => {
  const config = returnFetchConfig(dispatch, getState, pushFn, "GET");
  if (config) {
    fetch(`${devUrl}/api/me`, config)
      .then(res => {
        return res.json();
      })
      .then(me => {
        dispatch(storeMe(me));
      });
  }
};

const fetchFeed = pushFn => (dispatch, getState) => {
  const config = returnFetchConfig(dispatch, getState, pushFn, "GET");

  if (!getState().user.id) {
    dispatch(fetchMe(pushFn));
  }

  if (config) {
    fetch(`${devUrl}/api/feed`, config)
      .then(res => {
        return res.json();
      })
      .then(feed => {
        // console.log("fetch feed api call", feed)
        dispatch(storeFeed(feed));
      });
  }
};

const toggleLike = postId => (dispatch, getState) => {
  const config = returnFetchConfig(dispatch, getState, null, "POST");
  // console.log("API Call toggle like", config);
  // console.log(postId)
  if (config) {
    fetch(`${devUrl}/api/blitzs/${postId}/like`, config)
      .then(res => {
        console.log(res);
      })
      .then(() => {
        dispatch(likePost(postId));
      });
  }
};

export { login, fetchFeed, fetchUserProfile, toggleLike };
