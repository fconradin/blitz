import {
  WRONG_CREDENTIALS,
  STORE_USER_AND_TOKEN,
  READ_USER_TOKEN,
  STORE_FEED,
  STORE_PROFILE,
  LIKE_POST,
  STORE_ME
} from "../constants.js";

const wrongCredentials = () => ({
  type: WRONG_CREDENTIALS
});

const storeUserAndToken = (user, token) => ({
  type: STORE_USER_AND_TOKEN,
  user,
  token
});

const readUserToken = () => ({
  type: READ_USER_TOKEN
});

const storeFeed = feed => ({
  type: STORE_FEED,
  feed
});

const storeProfile = profile => ({
  type: STORE_PROFILE,
  profile
});

const likePost = post => ({
  type: LIKE_POST,
  post
});

const storeMe = me => ({
  type:STORE_ME,
  me
})

export {
  wrongCredentials,
  storeUserAndToken,
  readUserToken,
  storeFeed,
  storeProfile,
  likePost,
  storeMe
};
