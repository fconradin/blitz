import { STORE_FEED } from "../constants.js";

const feed = (state = "", action) => {
  switch (action.type) {
    case STORE_FEED:
      return action.feed;
    default:
      return state;
  }
};

export default feed;
