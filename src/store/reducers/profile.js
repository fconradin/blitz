import { STORE_PROFILE } from "../constants.js";

const profile = (state = "", action) => {
  switch (action.type) {
    case STORE_PROFILE:
      return action.profile;
    default:
      return state;
  }
};

export default profile;