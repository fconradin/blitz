import {
  READ_USER_TOKEN,
  STORE_USER_AND_TOKEN,
  LIKE_POST,
  STORE_ME
} from "../constants.js";

const user = (state = { me: {}, token: "" }, action) => {
  switch (action.type) {
    case READ_USER_TOKEN: {
      // console.log("user reducer reading user and token");
      const token = JSON.parse(localStorage.getItem("token"));
      return { ...state, token };
    }
    case STORE_USER_AND_TOKEN: {
      const token = JSON.stringify(action.token);
      localStorage.setItem("token", token);
      return { me: action.user, token: action.token };
    }
    case LIKE_POST: {
      // console.log("in user reducer, post id should be here", state)
      let newLikes;
      if (state.me.likes.indexOf(action.post)>-1){
        newLikes = state.me.likes.filter(item=>item!==action.post)
      } else {
        newLikes = state.me.likes.concat(action.post)
      }
      // console.log("user reducer", newLikes);
      const newState = {
        ...state,
        me: { ...state.me, likes: newLikes }
      };
      // console.log(state.user.likes);
      // console.log(newState.user.likes);
      return newState;}
    case STORE_ME: {
      return {...state, me:action.me}
    }

    default:
      return state;
  }
};

export default user;
