import { WRONG_CREDENTIALS } from "../constants.js";

const credentialCheck = (state = "", action) => {
  switch (action.type) {
    case WRONG_CREDENTIALS:
      return "Wrong credentials";
    default:
      return state;
  }
};

export default credentialCheck;