import user from './user.js'
import feed from './feed.js'
import profile from './profile'
import credentialCheck from './credentialCheck.js'
import { combineReducers } from "redux";

const reducer = combineReducers({ user, credentialCheck, feed, profile });

export default reducer;

